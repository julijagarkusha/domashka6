/**
 * @method max
 * @param numbers {number[]}
 * @returns {number | undefined}
 */
function max(numbers) {
    if(!numbers.length) {
        return;
    }

    let numberMax = numbers[0];

    for(let i = 0; i < numbers.length; i++) {
        if(numberMax < numbers[i]) {
            numberMax = numbers[i]
        }
    }

    return numberMax;
}

/**
 * @method max2
 * @param numbers {number[]}
 * @returns {number | undefined}
 */
function max2(numbers) {
    if (!numbers.length) {
        return;
    }

    if(numbers.length === 1) {
        return numbers[0];
    }

    const newNumbers = numbers[0] > numbers.at(-1) ? numbers.slice(0, -1) : numbers.slice(1);

    return max2(newNumbers);
}

const containTestsData = [
    { array: [8], expectedResult: 8 },
    { array: [1, 8, 37, 5, 17], expectedResult: 37 },
    { array: [8, 17], expectedResult: 17 },
]

containTestsData.forEach(testData => {
    const {
        expectedResult,
        array,
    } = testData;

    //test
    console.log('---------------------------------------------------');
    console.log('|           max (cycle implementation)            |');
    console.log('---------------------------------------------------');
    const testResult = max(array);
    console.log('***************************');
    console.log(`array: ${JSON.stringify(testData.array)}`);
    console.log(`Function\`s result: ${String(testResult)}`);
    console.log(`Expected result: ${String(expectedResult)}`);
    console.log(`Test is ${testResult === expectedResult ? 'passed' : 'failed'}`);
})

containTestsData.forEach(testData => {
    const {
        expectedResult,
        array,
    } = testData;

    //test
    console.log('---------------------------------------------------');
    console.log('|         max (recursion implementation)          |');
    console.log('---------------------------------------------------');
    const testResult = max2(array);
    console.log('***************************');
    console.log(`array: ${JSON.stringify(testData.array)}`);
    console.log(`Function\`s result: ${String(testResult)}`);
    console.log(`Expected result: ${String(expectedResult)}`);
    console.log(`Test is ${testResult === expectedResult ? 'passed' : 'failed'}`);
})
